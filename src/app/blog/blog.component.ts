import { CommentService } from './../service/comment.service';
import { BlogService } from './../service/blog.service';
import { Comment } from './../models/comment.model';
import { Component, OnInit } from '@angular/core';
import { BlogPost } from '../models/blog-post.model';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css']
})
export class BlogComponent implements OnInit {
  authorName:string = "Helber Belmiro";
  authorLink ="https://dzone.com/users/1131057/hbelmiro.html";
  postDate:string= new Date(2021,2,7).toDateString();
  postTitle :string = "3 Principles for Java Engineers to Stay Up-to-date With Technology";
  part1:string = "Java Engineers can adopt several strategies to stay up-to-date with technology. One of the most natural ones is by developing frameworks and libraries. By doing that, they often find themselves in one of these two situations: Any significant change in their code would break their client’s applications. So, they struggle with releasing extra features. Most of their releases comprise only minor bug fixes. Although they often release versions containing extra features, their clients never upgrade. Upgrading to the recent version would take too much effort, and the clients don’t have the time for that. If you find yourself in one of those situations, you need to know the 3 principles that sustain great frameworks and libraries. Applying these principles in your APIs will put you on the path to staying up-to-date.";
  part2:string= "Enable Your Clients To Start Fast What do you do when you try a new framework or library? Do you read the entire documentation learning the theory before starting coding? Or you just find a simple use-case and then change and experiment? Most developers prefer the second option. They don’t want to read much documentation. They just want to get their things done. When you think about their managers, it’s the same. Managers want a short Time To Market. They want their software running in production in the shortest time possible. To achieve that, create APIs that are easy to understand by doing a quick look at the classes and methods. Try to keep them simple and give them meaningful names. Besides that, also add use-cases to the documentation. Most popular frameworks have guides like that. By having access to use-cases, the developer can find the one he needs and adapt it. Often, that’s all he needs to get his things done."
  part3:string ="Keep Your Product Up-To-Date With The Market Trends Developers don’t enjoy working with uncool stuff. They want to use the coolest technologies and share their knowledge with other developers. They like people to see them as ones who know a lot of cool stuff. That’s important because people need to know your product, or else nobody will use it. And being cool, it’ll have free and genuine publicity. So, what makes a framework or library cool? Besides being easy to get started with (Principle 1), it has to be modern and up-to-date with the market trends, releasing additional features with consistency. If you do that, you’ll also stay up-to-date with the new stuff. So everyone wins. But you have to do it right. That’s why you need to know the third and most important principle.";
  part4:string ="Enable Your Clients to Upgrade With Ease Total Cost of Ownership (TCO) is the price a client pays for a product, plus the price he pays to keep it up and running. That’s important because it’s connected with both previous principles. It connects with Principle 1 because the clients want to save money always as possible. They don’t want to spend much money to have their applications up and running. It also connects with Principle 2 because your product has to be cool. At some point, your clients will need the additional features you’ve been releasing. But just being cool isn’t enough. It also has to be backward compatible. Otherwise, the clients will need to change their applications when upgrading, and this will take time and money. Keeping backward compatibility is essential for a lasting product. Once your clients use your APIs, you can’t change them anymore. That’s why you need to design them carefully. Do it in a way that allows you to evolve without breaking the client’s applications. How do you do that? Define specific use-cases, as mentioned in Principle 1, and the early versions of your APIs shouldn’t be too flexible. Flexibility is inversely proportional to evolution with backward compatibility. So start rigid and then escalate the flexibility. Also, prefer to use final methods and classes whenever you can. You should also create high-cohesion and low-coupling artifacts. Remember that the more spread out an artifact is, the more difficult is to change it. And the most important tip I can give you for keeping backward compatibility is to encapsulate the internals. Make everything private and expose only the essential classes and methods that the client needs to use. Besides decreasing complexity, this will allow you to make changes without breaking compatibility.";
  part5:string ="Being able to apply the three principles of great frameworks and libraries in your APIs will certainly help you work with new technologies. It’s a must for you to evolve your product with consistency. Now it’s up to you. What’s the biggest challenge you’re facing right now about designing or maintaining an API? What’s preventing you from applying those principles and staying up-to-date with the new technologies? Leave a comment if you find this article interesting. I’ll be happy to help you overcome this challenge.";
  
  subTitle1:string ="Principle 1:";
  subTitle2:string ="Principle 2:";
  subTitle3:string ="Principle 3:";
  subTitle4:string ="Conclusion";
  aboutAuthor:string ="Software Engineer, Banrisul , -I help Java Engineers build frameworks and libraries that are easy to evolve and upgrade so they can stay up-to-date with technology and work on great projects.";

  posts : BlogPost[]=[];
  comments : Comment[]=[];

  constructor(private blogService:BlogService, private commentService:CommentService) { }

  ngOnInit(): void {
    this.getAllComments();
    this.getAllPosts();
  }

  getAllComments():void{
    this.comments =this.commentService.retrievAllComments();
    
  }
  getAllPosts(): void{
      this.posts= this.blogService.retrievAllPosts();
  }

}
