import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BlogComponent } from './blog/blog.component';
import { ContactComponent } from './contact/contact.component';
import { HomeComponent } from './home/home.component';
import { Page404Component } from './page404/page404.component';

const routes: Routes = [
  { path: "", component: HomeComponent }, // if path = www.domain.com
  { path: "blog", component: BlogComponent }, // if path = www.domain.com/blog
  { path: "contact", component: ContactComponent }, // if path = www.domain.com/blog
  { path: '**', component: Page404Component } // if path is erroneous for example: www.domain.com/contact
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
