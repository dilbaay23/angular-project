import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-intro',
  templateUrl: './intro.component.html',
  styleUrls: ['./intro.component.css']
})
export class IntroComponent implements OnInit {

  title :string ="My First Angular Project Ever"
  imagePath :string = "./assets/Tulips.jpg"

  constructor() { }

  ngOnInit(): void {
  }

}
