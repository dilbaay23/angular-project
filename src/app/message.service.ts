import { Message } from './messages/message.model';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MessageService {
  private message1 :Message= new Message("Iron Man","This is my first message","./assets/ironman.jfif");
  private message2 :Message= new Message("Black Widow","This is my second message","./assets/blackwidow.jfif");
  private message3 :Message= new Message("Hulk","This is my third message","./assets/hulk.jpg");
  
  private messages : Message[]=[];


  constructor() { 
    this.messages =[
      this.message1, this.message2,this.message3
    ];
  }

  retrievAllMessages() :Message[]{
    return this.messages;
  }
}
