import { Comment } from './../models/comment.model';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CommentService {
  private comment1 :Comment= new Comment("John Dean ","Wow this blog is very nice...");
  private comment2 :Comment= new Comment("Ronnie Shel ","Carry on, don’t stop...");
  private comment3 :Comment= new Comment("Saskia Frank","Thanks for the information...");
  
  private comments : Comment[]=[];


  constructor() { 
    this.comments =[
      this.comment1, this.comment2,this.comment3
    ];
  }

  retrievAllComments() :Comment[]{
    return this.comments;
  }
}