import { BlogPost } from './../models/blog-post.model';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BlogService {
  blogPost1 :BlogPost= new BlogPost("March 10, 2021 ","Multithreading Java and Interviews Part 3: Wait and Notify All","https://dzone.com/thumbnail?fid=7044036&w=80&caller=author-bio-service");
  blogPost2 :BlogPost= new BlogPost("February 09, 2021 ","Top 10 Articles in the Java Zone: January 2021","https://dzone.com/thumbnail?fid=9433467&w=80&caller=author-bio-service");
  blogPost3 :BlogPost= new BlogPost("March 12, 2021 ","RabbitMQ Installation | RabbitMQ Tutorial: Java, Java Spring [Video]","https://dzone.com/thumbnail?fid=14202624&w=80&caller=author-bio-service");
  
  
  posts : BlogPost[]=[];


  constructor() { 
    this.posts =[
      this.blogPost1, this.blogPost2,this.blogPost3
    ];
  }

  retrievAllPosts() :BlogPost[]{
    return this.posts;
  }
}