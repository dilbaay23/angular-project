import { MessageService } from './../message.service';
import { Message } from './message.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {


  
  messages : Message[]=[];

  title: string = "Messages";
  isVisible: boolean = false;

  

  constructor(private messageService: MessageService) { 

  }

  ngOnInit(): void {
    this.getAllMesages();
  }
  toggleVisibility(): void {
    this.isVisible = !this.isVisible
  }

  getAllMesages():void{
    this.messages= this.messageService.retrievAllMessages();
  }

}
